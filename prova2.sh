#!/bin/bash

while true 
do	
	echo '------------------------------Menu------------------------------
	Digite "a)" para sair do programa
	Digite "b)" para selecionar um arquivo
       	Digite "c)" para ver um preview do arquivo selecionado
	Digite "d)" criar um arquivo com N linhas de blablabla
	Digite "e)" para confirmar se um arquivo existe
	Digite "g)" para listar apenas os subdiretórios do diretório atual
	Digite "h)" para listar os arquivos do diretório atual
	Digite "i)" para mudar o diretório atual	
	Digite "j)" para listar os arquivos executáveis do diretório atual
	'
	read -p "Digite a opção: " opc
	if [ $opc = "a" ]
	then
	       echo "**************Finalizando**************"	
		break	 
	elif [ $opc = "b" ]
	then 
		read -p "Digite o nome de um arquivo: " arq
		echo "----->Arquivo $arq selecionado<-----"

	elif [ $opc = "c" ]
	then
		echo "----->Exibindo preview do arquivo $arq<-----" 
		head -n 2 $arq 
		echo "[...]"
		tail -n 2 $arq
	elif [ $opc = "d" ]
	then
		read -p "Digite a quantidade de linhas para criar o arquivo: " qtd
		echo "----->Criando arquivo BLABLABLA<-----"
		sort -R texto.txt | head -n $qtd | touch bla
	elif [ $opc = "e" ]
	then
		echo "----->Exibindo subdiretórios do diretório atual: $arq<-----"
		ls -d */
	elif [ $opc = "f" ]
	then
		echo "----->Exibindo o arquivo: $arq<-----"
		cat $arq
	elif [ $opc = "g" ]
	then
		echo "----->Arquivos do diretório atual<-----"
		ls -l | grep '^-'
	elif [ $opc = "h" ]
	then
		echo "----->Arquivos executáveis do diretório atual<-----"
		ls -l | grep '^-..x'
	elif [ $opc = "i" ]
	then
		read -p "Digite o diretório para abrir: " dir
		echo "---->Mudando de diretório<-----"	
		echo .
		sleep 1
		echo .
		sleep 2
		echo .
		cd $dir
	else
		echo .
		sleep 1
		echo .
		sleep 1
		echo .
		sleep 1
		echo .

		echo "----->Opção não encontrada<-----"
	fi

done

